﻿using GAMongo.Core.Caching;
using GAMongo.Core.Domain.Tasks;
using GAMongo.Core.Infrastructure;
using GAMongo.Services.Logging;
using GAMongo.Services.Tasks;

namespace GAMongo.Services.Tasks
{
    /// <summary>
    /// Represents a task to clear [Log] table
    /// </summary>
    public partial class ClearLogScheduleTask : ScheduleTask, IScheduleTask
    {
        private readonly ILogger _logger;
        private readonly object _lock = new object();
        public ClearLogScheduleTask(ILogger logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            lock (_lock)
            {
                _logger.ClearLog();
            }
        }
    }
}
