using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GAMongo.Core;
using GAMongo.Core.Data;
using GAMongo.Core.Domain;
using GAMongo.Core.Domain.Customers;
using GAMongo.Core.Domain.Logging;
using GAMongo.Core.Domain.Orders;
using GAMongo.Core.Domain.Security;
using GAMongo.Core.Domain.Tasks;
using GAMongo.Core.Infrastructure;
using GAMongo.Services.Customers;
using GAMongo.Services.Helpers;
using MongoDB.Bson;
using MongoDB.Driver;
using GAMongo.Core.Domain.Configuration;
using GAMongo.Services.Tasks;
using GAMongo.Services.Security;
using GAMongo.Core.Caching;
using GAMongo.Services.Events;
using Microsoft.AspNetCore.Hosting;

namespace GAMongo.Services.Installation
{
    public partial class CodeFirstInstallationService : IInstallationService
    {
        #region Fields

        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Log> _logRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<CustomerRole> _customerRoleRepository;
        private readonly IRepository<ActivityLogType> _activityLogTypeRepository;
        private readonly IRepository<ActivityLog> _activityLogRepository;
        private readonly IRepository<ScheduleTask> _scheduleTaskRepository;
        private readonly IRepository<Setting> _settingRepository;
        private readonly IRepository<PermissionRecord> _permissionRepository;
        private readonly IWebHelper _webHelper;

        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region Ctor

        public CodeFirstInstallationService(
            IRepository<Order> orderRepository,
            IRepository<Log> logRepository,
            IRepository<Customer> customerRepository,
            IRepository<CustomerRole> customerRoleRepository,
            IRepository<ActivityLogType> activityLogTypeRepository,
            IRepository<ActivityLog> activityLogRepository,
            IRepository<ScheduleTask> scheduleTaskRepository,
            IRepository<Setting> settingRepository,
            IRepository<PermissionRecord> permissionRepository,
            IWebHelper webHelper,
            IHostingEnvironment hostingEnvironment)
        {
            
            this._logRepository = logRepository;
            this._customerRepository = customerRepository;
            this._customerRoleRepository = customerRoleRepository;            
            this._activityLogTypeRepository = activityLogTypeRepository;
            this._activityLogRepository = activityLogRepository;           
            this._scheduleTaskRepository = scheduleTaskRepository;
            this._settingRepository = settingRepository;
            this._permissionRepository = permissionRepository;
            this._webHelper = webHelper;
            this._hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Utilities

        protected virtual string GetSamplesPath()
        {
            return Path.Combine(_hostingEnvironment.WebRootPath, "Content/samples/");
        }

       
        protected virtual void InstallCustomersAndUsers(string defaultUserEmail, string defaultUserPassword)
        {
            var crAdministrators = new CustomerRole
            {
                Name = "Administrators",
                Active = true,
                IsSystemRole = true,
                SystemName = SystemCustomerRoleNames.Administrators,
            };
            _customerRoleRepository.Insert(crAdministrators);
          
            var crRegistered = new CustomerRole
            {
                Name = "Registered",
                Active = true,
                IsSystemRole = true,
                SystemName = SystemCustomerRoleNames.Registered,
            };
            _customerRoleRepository.Insert(crRegistered);

            var crGuests = new CustomerRole
            {
                Name = "Guests",
                Active = true,
                IsSystemRole = true,
                SystemName = SystemCustomerRoleNames.Guests,
            };
            _customerRoleRepository.Insert(crGuests);

            //admin user
            var adminUser = new Customer
            {
                CustomerGuid = Guid.NewGuid(),
                Username = defaultUserEmail,
                Password = defaultUserPassword,
                PasswordFormat = PasswordFormat.Clear,
                PasswordSalt = "",
                Active = true,
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
                PasswordChangeDateUtc = DateTime.UtcNow,
            };
           
            adminUser.CustomerRoles.Add(crAdministrators);
            adminUser.CustomerRoles.Add(crRegistered);
            _customerRepository.Insert(adminUser);

            //search engine (crawler) built-in user
            var searchEngineUser = new Customer
            {
                CustomerGuid = Guid.NewGuid(),
                PasswordFormat = PasswordFormat.Clear,
                AdminComment = "Built-in system guest record used for requests from search engines.",
                Active = true,
                IsSystemAccount = true,
                SystemName = SystemCustomerNames.BackgroundTask,
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
            };
            searchEngineUser.CustomerRoles.Add(crGuests);
            _customerRepository.Insert(searchEngineUser);

            //built-in user for background tasks
            var backgroundTaskUser = new Customer
            {
                CustomerGuid = Guid.NewGuid(),
                PasswordFormat = PasswordFormat.Clear,
                AdminComment = "Built-in system record used for background tasks.",
                Active = true,
                IsSystemAccount = true,
                SystemName = SystemCustomerNames.BackgroundTask,
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
            };
            backgroundTaskUser.CustomerRoles.Add(crGuests);
            _customerRepository.Insert(backgroundTaskUser);

        }       

        protected virtual void InstallActivityLogTypes()
        {
            var activityLogTypes = new List<ActivityLogType>
                                      {
                                          //admin area activities
                                         new ActivityLogType
                                              {
                                                  SystemKeyword = "AddNewCustomer",
                                                  Enabled = true,
                                                  Name = "Add a new customer"
                                              },
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "AddNewCustomerRole",
                                                  Enabled = true,
                                                  Name = "Add a new customer role"
                                              },
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "AddNewDiscount",
                                                  Enabled = true,
                                                  Name = "Add a new discount"
                                              },
                                        
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "AddNewSetting",
                                                  Enabled = true,
                                                  Name = "Add a new setting"
                                              },                                          
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "DeleteCustomer",
                                                  Enabled = true,
                                                  Name = "Delete a customer"
                                              },
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "DeleteCustomerRole",
                                                  Enabled = true,
                                                  Name = "Delete a customer role"
                                              },                                         
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "EditCustomer",
                                                  Enabled = true,
                                                  Name = "Edit a customer"
                                              },
                                          new ActivityLogType
                                              {
                                                  SystemKeyword = "EditCustomerRole",
                                                  Enabled = true,
                                                  Name = "Edit a customer role"
                                              },
                                         
                                      };
            _activityLogTypeRepository.Insert(activityLogTypes);
        }

       
        protected virtual void InstallScheduleTasks()
        {
            //these tasks are default - they are created in order to insert them into database
            //and nothing above it
            //there is no need to send arguments into ctor - all are null
            var tasks = new List<ScheduleTask>
            {
                new ScheduleTask
                {
                    ScheduleTaskName = "Delete guests",
                    Type = "GAMongo.Services.Tasks.DeleteGuestsScheduleTask, GAMongo.Services",
                    Enabled = true,
                    StopOnError = false,
                    TimeIntervalChoice = TimeIntervalChoice.EveryMinutes,
                    TimeInterval = 10,
                    MinuteOfHour = 1,
                    HourOfDay = 1,
                    DayOfWeek  = DayOfWeek.Thursday,
                    MonthOptionChoice = MonthOptionChoice.OnSpecificDay,
                    DayOfMonth = 1
                },
                new ScheduleTask
                {
                    ScheduleTaskName = "Clear log",
                    Type = "GAMongo.Services.Tasks.ClearLogScheduleTask",
                    Enabled = false,
                    StopOnError = false,
                    TimeIntervalChoice = TimeIntervalChoice.EveryHours,
                    TimeInterval = 1,
                    MinuteOfHour = 1,
                    HourOfDay = 1,
                    DayOfWeek  = DayOfWeek.Thursday,
                    MonthOptionChoice = MonthOptionChoice.OnSpecificDay,
                    DayOfMonth = 1
                },                
            };
            _scheduleTaskRepository.Insert(tasks);
        }

       
       

        

        private void CreateIndexes()
        {
            //customer
            _customerRepository.Collection.Indexes.CreateOneAsync(Builders<Customer>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });
            _customerRepository.Collection.Indexes.CreateOneAsync(Builders<Customer>.IndexKeys.Descending(x => x.CreatedOnUtc).Ascending(x => x.Deleted).Ascending("CustomerRoles._id"), new CreateIndexOptions() { Name = "CreatedOnUtc_1_CustomerRoles._id_1", Unique = false });
            _customerRepository.Collection.Indexes.CreateOneAsync(Builders<Customer>.IndexKeys.Ascending(x => x.LastActivityDateUtc), new CreateIndexOptions() { Name = "LastActivityDateUtc_1", Unique = false });
            _customerRepository.Collection.Indexes.CreateOneAsync(Builders<Customer>.IndexKeys.Ascending(x => x.CustomerGuid), new CreateIndexOptions() { Name = "CustomerGuid_1", Unique = false });

            //customer role
            _customerRoleRepository.Collection.Indexes.CreateOneAsync(Builders<CustomerRole>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });


            // Activity log type
            _activityLogTypeRepository.Collection.Indexes.CreateOneAsync(Builders<ActivityLogType>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });

            // Activity log 
            _activityLogRepository.Collection.Indexes.CreateOneAsync(Builders<ActivityLog>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });

            //Log
            _logRepository.Collection.Indexes.CreateOneAsync(Builders<Log>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });
            _logRepository.Collection.Indexes.CreateOneAsync(Builders<Log>.IndexKeys.Descending(x => x.CreatedOnUtc), new CreateIndexOptions() { Name = "CreatedOnUtc", Unique = false });


            //setting
            _settingRepository.Collection.Indexes.CreateOneAsync(Builders<Setting>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });
            _settingRepository.Collection.Indexes.CreateOneAsync(Builders<Setting>.IndexKeys.Ascending(x => x.Name), new CreateIndexOptions() { Name = "Name", Unique = false });


            //order
            _orderRepository.Collection.Indexes.CreateOneAsync(Builders<Order>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });
            _orderRepository.Collection.Indexes.CreateOneAsync(Builders<Order>.IndexKeys.Ascending(x => x.CustomerId).Descending(x => x.CreatedOnUtc), new CreateIndexOptions() { Name = "CustomerId_1_CreatedOnUtc_-1", Unique = false });
            _orderRepository.Collection.Indexes.CreateOneAsync(Builders<Order>.IndexKeys.Descending(x => x.CreatedOnUtc), new CreateIndexOptions() { Name = "CreatedOnUtc_-1", Unique = false });
            _orderRepository.Collection.Indexes.CreateOneAsync(Builders<Order>.IndexKeys.Ascending("OrderItems.ProductId"), new CreateIndexOptions() { Name = "OrderItemsProductId" });
            _orderRepository.Collection.Indexes.CreateOneAsync(Builders<Order>.IndexKeys.Ascending("OrderItems._id"), new CreateIndexOptions() { Name = "OrderItemId" });

            //permision
            _permissionRepository.Collection.Indexes.CreateOneAsync(Builders<PermissionRecord>.IndexKeys.Ascending(x => x.Id), new CreateIndexOptions() { Name = "Id", Unique = true });

        }

        #endregion

        #region Methods


        public virtual void InstallData(string defaultUserEmail,
            string defaultUserPassword, bool installSampleData = true)
        {

            defaultUserEmail = defaultUserEmail.ToLower();

            CreateIndexes();
            
            InstallCustomersAndUsers(defaultUserEmail, defaultUserPassword);            
            InstallActivityLogTypes();            
           
        }


        #endregion

    }
}
