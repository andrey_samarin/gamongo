
namespace GAMongo.Core.Domain.Customers
{
    public static partial class SystemCustomerNames
    {
        public static string BackgroundTask { get { return "BackgroundTask"; } }
        public static string WebApi { get { return "WebApi"; } }
    }
}