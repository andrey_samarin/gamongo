﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAMongo.Core.Domain.Customers
{
    public enum PasswordFormat
    {
        Clear = 0,
        Hashed = 1,
        Encrypted = 2
    }
}
