namespace GAMongo.Core.Domain.Customers
{
    /// <summary>
    /// Represents the customer registration type fortatting enumeration
    /// </summary>
    public enum UserRegistrationType
    {
        /// <summary>
        /// Standard account creation
        /// </summary>
        Standard = 1,
        /// <summary>
        /// A customer should be approved by administrator
        /// </summary>
        AdminApproval = 2,
        /// <summary>
        /// Registration is disabled
        /// </summary>
        Disabled = 3,
    }
}
