﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAMongo.Core.Domain.Tasks
{
    public enum TimeIntervalChoice
    {
        /// <summary>
        /// Every minutes
        /// </summary>
        EveryMinutes = 10,

        /// <summary>
        /// Every hours
        /// </summary>
        EveryHours = 20,

        /// <summary>
        /// Every days
        /// </summary>
        EveryDays = 30,

        /// <summary>
        /// Every weeks
        /// </summary>
        EveryWeeks = 40,

        /// <summary>
        /// Every Months
        /// </summary>
        EveryMonths = 50
    }
}
