﻿namespace GAMongo.Framework.UI
{
    public enum NotifyType
    {
        Success,
        Error,
        Warning
    }
}
