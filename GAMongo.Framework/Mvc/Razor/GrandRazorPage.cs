﻿using GAMongo.Core;
using GAMongo.Core.Infrastructure;
namespace GAMongo.Framework.Mvc.Razor
{
    /// <summary>
    /// Web view page
    /// </summary>
    /// <typeparam name="TModel">Model</typeparam>
    public abstract class GrandRazorPage<TModel> : Microsoft.AspNetCore.Mvc.Razor.RazorPage<TModel>
    {
        private IWorkContext _workContext;

        public IWorkContext WorkContext
        {
            get
            {
                if (_workContext == null)
                    _workContext = EngineContext.Current.Resolve<IWorkContext>();
                return _workContext;
            }
        }
       
        /// <summary>
        /// Gets a selected tab index (used in admin area to store selected tab index)
        /// </summary>
        /// <returns>Index</returns>
        public int GetSelectedTabIndex()
        {
            //keep this method synchornized with
            //"SetSelectedTabIndex" method of \Administration\Controllers\BaseGrandController.cs
            int index = 0;
            string dataKey = "Grand.selected-tab-index";
            if (ViewData[dataKey] is int)
            {
                index = (int)ViewData[dataKey];
            }
            if (TempData[dataKey] is int)
            {
                index = (int)TempData[dataKey];
            }

            //ensure it's not negative
            if (index < 0)
                index = 0;

            return index;
        }

    }

    /// <summary>
    /// Web view page
    /// </summary>
    public abstract class GrandRazorPage : GrandRazorPage<dynamic>
    {
    }
}