﻿using Microsoft.AspNetCore.Mvc;

namespace GAMongo.Framework.Mvc
{
    public class NullJsonResult : JsonResult
    {
        public NullJsonResult() : base(null)
        {
        }
    }
}
