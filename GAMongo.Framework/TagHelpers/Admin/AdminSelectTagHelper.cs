﻿using GAMongo.Core;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

namespace GAMongo.Framework.TagHelpers.Admin
{

    [HtmlTargetElement("admin-select", Attributes = ForAttributeName)]
    public class AdminSelectTagHelper : Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper
    {
        private const string ForAttributeName = "asp-for";
        private const string DisplayHintAttributeName = "asp-display-hint";

        private readonly IWorkContext _workContext;

        public AdminSelectTagHelper(IHtmlGenerator generator, IWorkContext workContext) : base(generator)
        {
            _workContext = workContext;
        }

        [HtmlAttributeName(DisplayHintAttributeName)]
        public bool DisplayHint { get; set; } = true;

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            await base.ProcessAsync(context, output);
            output.TagName = "select";
            output.TagMode = TagMode.StartTagAndEndTag;
            var classValue = output.Attributes.ContainsName("class")
                                ? $"{output.Attributes["class"].Value}"
                                : "form-control k-input";
            output.Attributes.SetAttribute("class", classValue);
            
        }
    }

    
}