﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using GAMongo.Framework.Mvc.Models;
using GAMongo.Web.Validators.Install;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;

namespace GAMongo.Web.Models.Install
{
    [Validator(typeof(InstallValidator))]
    public partial class InstallModel : BaseGrandModel
    {
        public InstallModel()
        {
            this.AvailableLanguages = new List<SelectListItem>();
        }

        public string AdminEmail { get; set; }

        [NoTrim]
        [DataType(DataType.Password)]
        public string AdminPassword { get; set; }

        [NoTrim]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }


        public string DatabaseConnectionString { get; set; }

        public string DataProvider { get; set; }

        public bool MongoDBConnectionInfo { get; set; }
        public string MongoDBServerName { get; set; }
        public string MongoDBDatabaseName { get; set; }
        public string MongoDBUsername { get; set; }
        public string MongoDBPassword { get; set; }
        public bool DisableSampleDataOption { get; set; }
        public bool InstallSampleData { get; set; }
        public bool Installed { get; set; }
        public List<SelectListItem> AvailableLanguages { get; set; }
    }
}