﻿using System.Collections.Generic;
using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Common
{
    public partial class SitemapModel : BaseGrandModel
    {
        public SitemapModel()
        {

        }

        public bool NewsEnabled { get; set; }
        public bool BlogEnabled { get; set; }
        public bool ForumEnabled { get; set; }
    }
}