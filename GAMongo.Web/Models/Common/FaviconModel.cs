﻿using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Common
{
    public partial class FaviconModel : BaseGrandModel
    {
        public string FaviconUrl { get; set; }
    }
}