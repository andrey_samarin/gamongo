﻿using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Common
{
    public partial class LogoModel : BaseGrandModel
    {
        public string StoreName { get; set; }

        public string LogoPath { get; set; }
    }
}