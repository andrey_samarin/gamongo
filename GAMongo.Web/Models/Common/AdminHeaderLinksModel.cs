﻿using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Common
{
    public partial class AdminHeaderLinksModel : BaseGrandModel
    {
        public string ImpersonatedCustomerEmailUsername { get; set; }
        public bool IsCustomerImpersonated { get; set; }
        public bool DisplayAdminLink { get; set; }
        public string EditPageUrl { get; set; }
    }
}