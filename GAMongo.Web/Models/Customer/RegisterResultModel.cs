﻿using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Customer
{
    public partial class RegisterResultModel : BaseGrandModel
    {
        public string Result { get; set; }
    }
}