﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using GAMongo.Framework;
using GAMongo.Framework.Mvc.Models;
using GAMongo.Web.Validators.Customer;
using GAMongo.Framework.Mvc.ModelBinding;

namespace GAMongo.Web.Models.Customer
{
    [Validator(typeof(PasswordRecoveryValidator))]
    public partial class PasswordRecoveryModel : BaseGrandModel
    {
        [GrandResourceDisplayName("Account.PasswordRecovery.Email")]
        public string Email { get; set; }

        public string Result { get; set; }
    }
}