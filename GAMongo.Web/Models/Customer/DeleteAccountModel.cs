﻿using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using GAMongo.Framework.Mvc.Models;
using GAMongo.Web.Validators.Customer;
using GAMongo.Framework.Mvc.ModelBinding;

namespace GAMongo.Web.Models.Customer
{
    [Validator(typeof(DeleteAccountValidator))]
    public partial class DeleteAccountModel : BaseGrandModel
    {
        [NoTrim]
        [DataType(DataType.Password)]
        [GrandResourceDisplayName("Account.DeleteAccount.Fields.Password")]
        public string Password { get; set; }

    }
}