﻿using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Customer
{
    public partial class ExternalAuthenticationMethodModel : BaseGrandModel
    {
        public string ViewComponentName { get; set; }
    }
}