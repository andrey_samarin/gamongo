﻿using GAMongo.Framework.Mvc.Models;

namespace GAMongo.Web.Models.Customer
{
    public partial class AccountActivationModel : BaseGrandModel
    {
        public string Result { get; set; }
    }
}