﻿using FluentValidation;
using GAMongo.Framework.Validators;
using GAMongo.Web.Models.Customer;

namespace GAMongo.Web.Validators.Customer
{
    public class DeleteAccountValidator : BaseGrandValidator<DeleteAccountModel>
    {
        public DeleteAccountValidator()
        {
            RuleFor(x => x.Password).NotEmpty().WithMessage("Required");
        }}
}