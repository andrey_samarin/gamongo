﻿using FluentValidation;
using GAMongo.Framework.Validators;
using GAMongo.Web.Models.Customer;

namespace GAMongo.Web.Validators.Customer
{
    public class PasswordRecoveryValidator : BaseGrandValidator<PasswordRecoveryModel>
    {
        public PasswordRecoveryValidator()
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage("Required");
            RuleFor(x => x.Email).EmailAddress().WithMessage("WrongEmail");
        }}
}