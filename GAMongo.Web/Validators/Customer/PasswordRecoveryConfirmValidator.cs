﻿using FluentValidation;
using GAMongo.Framework.Validators;
using GAMongo.Web.Models.Customer;

namespace GAMongo.Web.Validators.Customer
{
    public class PasswordRecoveryConfirmValidator : BaseGrandValidator<PasswordRecoveryConfirmModel>
    {
        public PasswordRecoveryConfirmValidator()
        {
            RuleFor(x => x.NewPassword).NotEmpty().WithMessage("Required");
            RuleFor(x => x.NewPassword).Length(8, 999).WithMessage(string.Format("LengthValidation"));
        }}
}