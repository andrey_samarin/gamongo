﻿using Microsoft.AspNetCore.Mvc;

namespace GAMongo.Web.ViewComponents
{
    public class JavaScriptDisabledWarningViewComponent : ViewComponent
    {

        public JavaScriptDisabledWarningViewComponent()
        {

        }

        public IViewComponentResult Invoke()
        {

            return View();
        }
    }
}