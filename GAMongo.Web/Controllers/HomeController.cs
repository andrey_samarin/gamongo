﻿using Microsoft.AspNetCore.Mvc;
using GAMongo.Framework.Security;
using GAMongo.Framework.Mvc.Filters;

namespace GAMongo.WebControllers
{
    public partial class HomeController : Controller
    {
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Index()
        {
            return View();
        }
    }
}
