﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using GAMongo.Framework.Mvc.Routing;

namespace GAMongo.Web.Infrastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            //areas
            routeBuilder.MapRoute(name: "areaRoute", template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

            //home page
            routeBuilder.MapRoute("HomePage", "", new { controller = "Home", action = "Index" });

            //widgets
            //we have this route for performance optimization because named routeBuilder are MUCH faster than usual Html.Action(...)
            //and this route is highly used
            routeBuilder.MapRoute("WidgetsByZone",
                            "widgetsbyzone/",
                            new { controller = "Widget", action = "WidgetsByZone" });
            //login
            routeBuilder.MapRoute("Login",
                            "login/",
                            new { controller = "Customer", action = "Login" });
            //logout
            routeBuilder.MapRoute("Logout",
                            "logout/",
                            new { controller = "Customer", action = "Logout" });
          
            //install
            routeBuilder.MapRoute("Installation", "install",
                            new { controller = "Install", action = "Index" });
            //page not found
            routeBuilder.MapRoute("PageNotFound", "page-not-found",
                            new { controller = "Common", action = "PageNotFound" });
        }

        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
