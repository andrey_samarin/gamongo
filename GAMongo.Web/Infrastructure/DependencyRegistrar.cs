using Autofac;
using GAMongo.Core.Configuration;
using GAMongo.Core.Infrastructure;
using GAMongo.Core.Infrastructure.DependencyManagement;
using GAMongo.Framework.Controllers;
using GAMongo.Web.Infrastructure.Installation;


namespace GAMongo.Web.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, GrandConfig config)
        {
            //installation localization service
            builder.RegisterType<InstallationLocalizationService>().As<IInstallationLocalizationService>().InstancePerLifetimeScope();


        }

        public int Order
        {
            get { return 2; }
        }
    }
}
