﻿using FluentValidation;
using GAMongo.Web.Areas.Admin.Models.Plugins;
using GAMongo.Framework.Validators;

namespace GAMongo.Web.Areas.Admin.Validators.Plugins
{
    public class PluginValidator : BaseGrandValidator<PluginModel>
    {
        public PluginValidator()
        {
            RuleFor(x => x.FriendlyName).NotEmpty().WithMessage("Required");
        }
    }
}