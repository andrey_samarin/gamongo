﻿using FluentValidation;
using GAMongo.Web.Areas.Admin.Models.Customers;
using GAMongo.Framework.Validators;

namespace GAMongo.Web.Areas.Admin.Validators.Customers
{
    public class CustomerRoleValidator : BaseGrandValidator<CustomerRoleModel>
    {
        public CustomerRoleValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Required");
        }
    }
}