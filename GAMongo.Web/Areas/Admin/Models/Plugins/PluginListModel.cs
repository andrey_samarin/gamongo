﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Plugins
{
    public partial class PluginListModel : BaseGrandModel
    {
        public PluginListModel()
        {
            AvailableLoadModes = new List<SelectListItem>();
            AvailableGroups = new List<SelectListItem>();
        }
        public int SearchLoadModeId { get; set; }
        public string SearchGroup { get; set; }
        public IList<SelectListItem> AvailableLoadModes { get; set; }
        public IList<SelectListItem> AvailableGroups { get; set; }
    }
}