﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Plugins
{
    public partial class OfficialFeedListModel : BaseGrandModel
    {
        public OfficialFeedListModel()
        {
            AvailableVersions = new List<SelectListItem>();
            AvailableCategories = new List<SelectListItem>();
        }
      
        public string SearchName { get; set; }
        public int SearchVersionId { get; set; }
        public string SearchCategoryId { get; set; }
        public IList<SelectListItem> AvailableVersions { get; set; }
        public IList<SelectListItem> AvailableCategories { get; set; }

        #region Nested classes

        public partial class ItemOverview
        {
            public string Url { get; set; }
            public string Name { get; set; }
            public string CategoryName { get; set; }
            public string SupportedVersions { get; set; }
            public string PictureUrl { get; set; }
        }

        #endregion
    }
}