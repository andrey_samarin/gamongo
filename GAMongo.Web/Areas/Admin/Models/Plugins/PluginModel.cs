﻿using GAMongo.Framework.Mvc.Models;
using FluentValidation.Attributes;
using GAMongo.Web.Areas.Admin.Validators.Plugins;

namespace GAMongo.Web.Areas.Admin.Models.Plugins
{
    [Validator(typeof(PluginValidator))]
    public partial class PluginModel : BaseGrandModel
    {
        public PluginModel()
        {

        }
       
        public string Group { get; set; }     
        public string FriendlyName { get; set; }        
        public string SystemName { get; set; }        
        public string Version { get; set; }       
        public string Author { get; set; }
        public int DisplayOrder { get; set; }
        public string ConfigurationUrl { get; set; }
        public bool Installed { get; set; }
        public bool CanChangeEnabled { get; set; }
        public bool IsEnabled { get; set; }
        public string LogoUrl { get; set; }
    }   
}