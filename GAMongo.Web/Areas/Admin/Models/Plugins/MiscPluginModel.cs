﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Plugins
{
    public partial class MiscPluginModel : BaseGrandModel
    {
        public string FriendlyName { get; set; }
        public string ConfigurationUrl { get; set; }
    }
}