﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Security
{
    public partial class PermissionRecordModel : BaseGrandModel
    {
        public string Name { get; set; }
        public string SystemName { get; set; }
    }
}