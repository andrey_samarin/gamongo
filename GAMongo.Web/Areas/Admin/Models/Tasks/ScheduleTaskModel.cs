﻿using GAMongo.Framework.Mvc.Models;
using GAMongo.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using GAMongo.Web.Areas.Admin.Validators.Tasks;
using System;

namespace GAMongo.Web.Areas.Admin.Models.Tasks
{
    [Validator(typeof(ScheduleTaskValidator))]
    public partial class ScheduleTaskModel : BaseGrandEntityModel
    {
        public string ScheduleTaskName { get; set; }
        public string LeasedByMachineName { get; set; }
        public string Type { get; set; }
        public bool Enabled { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.StopOnError")]
        public bool StopOnError { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.LastStartUtc")]
        public DateTime? LastStartUtc { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.LastEndUtc")]
        public DateTime? LastEndUtc { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.LastSuccessUtc")]
        public DateTime? LastSuccessUtc { get; set; }

        //Properties below are for FluentScheduler
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.TimeIntervalChoice")]
        public int TimeIntervalChoice { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.TimeInterval")]
        public int TimeInterval { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.MinuteOfHour")]
        public int MinuteOfHour { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.HourOfDay")]
        public int HourOfDay { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.DayOfWeek")]
        public int DayOfWeek { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.MonthOptionChoice")]
        public int MonthOptionChoice { get; set; }
        [GrandResourceDisplayName("Admin.System.ScheduleTasks.DayOfMonth")]
        public int DayOfMonth { get; set; }
    }
}