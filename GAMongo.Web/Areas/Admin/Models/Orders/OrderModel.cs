﻿using GAMongo.Framework.Mvc.Models;
using GAMongo.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace GAMongo.Web.Areas.Admin.Models.Orders
{
    public partial class OrderModel : BaseGrandEntityModel
    {
        public OrderModel()
        {

            Items = new List<OrderItemModel>();
        }

        public override string Id { get; set; }


        public IList<OrderItemModel> Items { get; set; }

        public int OrderNumber { get; set; }

        [GrandResourceDisplayName("Admin.Orders.Fields.OrderGuid")]
        public Guid OrderGuid { get; set; }

        //store
        [GrandResourceDisplayName("Admin.Orders.Fields.Store")]
        public string StoreName { get; set; }

        //customer info
        [GrandResourceDisplayName("Admin.Orders.Fields.Customer")]
        public string CustomerId { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.Customer")]
        public string CustomerInfo { get; set; }

        public string CustomerFullName { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.CustomerIP")]
        public string CustomerIp { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.UrlReferrer")]
        public string UrlReferrer { get; set; }

        //Used discounts

        public string OrderTotal { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.RefundedAmount")]
        public decimal TaxValue { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.Edit.TaxRates")]
        public string TaxRatesValue { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.Edit.OrderTotalDiscount")]
        public decimal OrderTotalDiscountValue { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.Edit.OrderTotal")]
        public decimal OrderTotalValue { get; set; }

        //order status
        [GrandResourceDisplayName("Admin.Orders.Fields.OrderStatus")]
        public string OrderStatus { get; set; }
        [GrandResourceDisplayName("Admin.Orders.Fields.OrderStatus")]
        public int OrderStatusId { get; set; }

        //creation date
        [GrandResourceDisplayName("Admin.Orders.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        public partial class OrderItemModel : BaseGrandEntityModel
        {
            public OrderItemModel()
            {

            }
            public string ProductId { get; set; }
            public string ProductName { get; set; }
            public string VendorName { get; set; }
            public string Sku { get; set; }
            public string UnitPriceInclTax { get; set; }
            public string UnitPriceExclTax { get; set; }
            public decimal UnitPriceInclTaxValue { get; set; }
            public decimal UnitPriceExclTaxValue { get; set; }
            public int Quantity { get; set; }
            public string DiscountInclTax { get; set; }
            public string DiscountExclTax { get; set; }
            public decimal DiscountInclTaxValue { get; set; }
            public decimal DiscountExclTaxValue { get; set; }
            public string SubTotalInclTax { get; set; }
            public string SubTotalExclTax { get; set; }
            public decimal SubTotalInclTaxValue { get; set; }
            public decimal SubTotalExclTaxValue { get; set; }

        }
    }

}