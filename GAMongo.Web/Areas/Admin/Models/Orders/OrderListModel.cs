﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Orders
{
    public partial class OrderListModel : BaseGrandModel
    {
        public OrderListModel()
        {
            AvailableOrderStatuses = new List<SelectListItem>();

        }

        [GrandResourceDisplayName("Admin.Orders.List.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [GrandResourceDisplayName("Admin.Orders.List.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [GrandResourceDisplayName("Admin.Orders.List.OrderStatus")]
        public int OrderStatusId { get; set; }

        [GrandResourceDisplayName("Admin.Orders.List.OrderGuid")]
        
        public string OrderGuid { get; set; }

        [GrandResourceDisplayName("Admin.Orders.List.GoDirectlyToNumber")]

        public int GoDirectlyToNumber { get; set; }

        public IList<SelectListItem> AvailableOrderStatuses { get; set; }
    }
}