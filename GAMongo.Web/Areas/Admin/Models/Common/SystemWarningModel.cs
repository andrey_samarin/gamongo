﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Common
{
    public partial class SystemWarningModel : BaseGrandModel
    {
        public SystemWarningLevel Level { get; set; }

        public string Text { get; set; }
    }

    public enum SystemWarningLevel
    {
        Pass,
        Warning,
        Fail
    }
}