﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Common
{
    public partial class UrlRecordListModel : BaseGrandModel
    {
        [GrandResourceDisplayName("Admin.System.SeNames.Name")]
        
        public string SeName { get; set; }
    }
}