using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Logging
{
    public partial class ActivityLogSearchModel : BaseGrandModel
    {
        public ActivityLogSearchModel()
        {
            ActivityLogType = new List<SelectListItem>();
        }
        [UIHint("DateNullable")]
        public DateTime? CreatedOnFrom { get; set; }
        [UIHint("DateNullable")]
        public DateTime? CreatedOnTo { get; set; }
        public string ActivityLogTypeId { get; set; }
        public IList<SelectListItem> ActivityLogType { get; set; }
        public string IpAddress { get; set; }

    }
}