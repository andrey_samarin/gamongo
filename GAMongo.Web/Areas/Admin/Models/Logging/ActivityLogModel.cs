﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Logging
{
    public partial class ActivityLogModel : BaseGrandEntityModel
    {
        public string ActivityLogTypeName { get; set; }
        public string ActivityLogTypeId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerEmail { get; set; }
        public string Comment { get; set; }
        public string IpAddress { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public partial class ActivityStatsModel : BaseGrandEntityModel
    {
        public string ActivityLogTypeName { get; set; }
        public string ActivityLogTypeId { get; set; }
        public string EntityKeyId { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }

    }
}
