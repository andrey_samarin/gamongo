﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Logging
{
    public partial class ActivityLogTypeModel : BaseGrandEntityModel
    {
        public string Name { get; set; }
        public bool Enabled { get; set; }
    }
}