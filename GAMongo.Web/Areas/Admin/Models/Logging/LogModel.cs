﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System;

using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Logging
{
    public partial class LogModel : BaseGrandEntityModel
    {

        public string LogLevel { get; set; }        
        public string ShortMessage { get; set; }        
        public string FullMessage { get; set; }       
        public string IpAddress { get; set; }
        public string CustomerId { get; set; }        
        public string PageUrl { get; set; }        
        public string ReferrerUrl { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}