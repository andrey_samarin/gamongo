﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Customers
{
    public partial class RegisteredCustomerReportLineModel : BaseGrandModel
    {
        public string Period { get; set; }
        public int Customers { get; set; }
    }
}