﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Customers
{
    public partial class OnlineCustomerModel : BaseGrandEntityModel
    {
        public string CustomerInfo { get; set; }
        public string LastIpAddress { get; set; }
        public string Location { get; set; }
        public DateTime LastActivityDate { get; set; }        
        public string LastVisitedPage { get; set; }
    }
}