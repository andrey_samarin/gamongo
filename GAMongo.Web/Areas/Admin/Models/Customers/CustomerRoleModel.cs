﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using GAMongo.Web.Areas.Admin.Validators.Customers;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;

namespace GAMongo.Web.Areas.Admin.Models.Customers
{
    [Validator(typeof(CustomerRoleValidator))]
    public partial class CustomerRoleModel : BaseGrandEntityModel
    {      
        public string Name { get; set; }
        public bool Active { get; set; }       
        public bool IsSystemRole { get; set; }
        public string SystemName { get; set; }
    }
}