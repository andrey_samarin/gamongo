﻿using GAMongo.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using GAMongo.Framework;
using GAMongo.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace GAMongo.Web.Areas.Admin.Models.Customers
{
    public partial class CustomerListModel : BaseGrandModel
    {
        public CustomerListModel()
        {
            SearchCustomerRoleIds = new List<string>();
            AvailableCustomerRoles = new List<SelectListItem>();
        }       
        public IList<SelectListItem> AvailableCustomerRoles { get; set; }

        [UIHint("MultiSelect")]
        public IList<string> SearchCustomerRoleIds { get; set; }
        public string SearchUsername { get; set; }

    }
}