﻿using GAMongo.Framework.Mvc.Models;
using GAMongo.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using GAMongo.Web.Areas.Admin.Validators.Customers;

namespace GAMongo.Web.Areas.Admin.Models.Customers
{
    [Validator(typeof(CustomerValidator))]
    public partial class CustomerModel : BaseGrandEntityModel
    {
        public CustomerModel()
        {
            this.AvailableCustomerRoles = new List<CustomerRoleModel>();
        }

        public bool AllowUsersToChangeUsernames { get; set; }
        public bool UsernamesEnabled { get; set; }       
        public string Username { get; set; }
       
        [DataType(DataType.Password)]
        [NoTrim]
        public string Password { get; set; }       
        public bool Active { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastActivityDate { get; set; }
        public string LastIpAddress { get; set; }
        public string LastVisitedPage { get; set; }
        public string LastUrlReferrer { get; set; }
        public string CustomerRoleNames { get; set; }
        public List<CustomerRoleModel> AvailableCustomerRoles { get; set; }
        public string[] SelectedCustomerRoleIds { get; set; }
    }
}