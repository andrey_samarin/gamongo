﻿using GAMongo.Core;
using GAMongo.Core.Domain.Customers;
using GAMongo.Core.Domain.Orders;
using GAMongo.Core.Infrastructure;
using GAMongo.Framework.Controllers;
using GAMongo.Framework.Extensions;
using GAMongo.Framework.Kendoui;
using GAMongo.Services.Customers;
using GAMongo.Services.Logging;
using GAMongo.Services.Orders;
using GAMongo.Services.Security;
using GAMongo.Web.Areas.Admin.Models.Orders;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;

namespace GAMongo.Web.Areas.Admin.Controllers
{
    public partial class OrderController : BaseAdminController
    {
        #region Fields

        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly IEncryptionService _encryptionService;
        private readonly IPermissionService _permissionService;
        private readonly ICustomerService _customerService;
        private readonly ICustomerActivityService _customerActivityService;
        #endregion

        #region Ctor

        public OrderController(IOrderService orderService, 
            IWorkContext workContext,
            IEncryptionService encryptionService,
            IPermissionService permissionService,
            ICustomerService customerService,
            ICustomerActivityService customerActivityService
            )
		{
            this._orderService = orderService;
            this._workContext = workContext;
            this._encryptionService = encryptionService;
            this._permissionService = permissionService;
            this._customerActivityService = customerActivityService;
        }

        #endregion


        [NonAction]
        protected virtual void PrepareOrderDetailsModel(OrderModel model, Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = order.Id;
            model.OrderNumber = order.OrderNumber;
            model.OrderStatus = order.OrderStatus.ToString();
            model.OrderStatusId = order.OrderStatusId;
            model.OrderGuid = order.OrderGuid;
            model.CustomerId = order.CustomerId;

            var customer = EngineContext.Current.Resolve<ICustomerService>().GetCustomerById(order.CustomerId);
            model.CustomerInfo = customer.IsRegistered() ? customer.Username : "Guest";
        }
      

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List(int? orderStatusId = null,
            int? paymentStatusId = null, int? shippingStatusId = null, DateTime? startDate = null)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            //order statuses
            var model = new OrderListModel();
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = "All", Value = " " });
            if (orderStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableOrderStatuses.FirstOrDefault(x => x.Value == orderStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            return View(model);
		}

		[HttpPost]
		public IActionResult OrderList(DataSourceRequest command, OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)model.StartDate.Value;

            DateTime? endDateValue = (model.EndDate == null) ? null 
                            :(DateTime?)model.EndDate.Value;

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;

            //load orders
            var orders = _orderService.SearchOrders(
                createdFromUtc: startDateValue, 
                createdToUtc: endDateValue,
                os: orderStatus,
                orderGuid: model.OrderGuid,
                pageIndex: command.Page - 1, 
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = orders.Select(x =>
                {
                    return new OrderModel
                    {
                        Id = x.Id,
                        OrderNumber = x.OrderNumber,
                        OrderTotal = x.OrderTotal.ToString(),
                        OrderStatus = x.OrderStatus.ToString(),
                        CustomerId = x.CustomerId,
                        CustomerFullName = string.Format("{0} {1}", "1", "2"),
                        CreatedOn = x.CreatedOnUtc,
                    };
                }),
                Total = orders.TotalCount
            };

            return Json(gridModel);
        }
        
        [HttpPost, ActionName("List")]
        [FormValueRequired("go-to-order-by-number")]
        public IActionResult GoToOrderId(OrderListModel model)
        {
            var order = _orderService.GetOrderByNumber(model.GoDirectlyToNumber);
            if (order == null)
                return List();
            
            return RedirectToAction("Edit", "Order", new { id = order.Id });
        }

    }
}
