﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using GAMongo.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using GAMongo.Web.Areas.Admin.Models.Logging;
using GAMongo.Core;
using GAMongo.Core.Domain.Logging;
using GAMongo.Services.Logging;
using GAMongo.Services.Security;
using GAMongo.Framework.Controllers;
using GAMongo.Framework.Kendoui;
using GAMongo.Core.Infrastructure;
using GAMongo.Services.Customers;

namespace GAMongo.Web.Areas.Admin.Controllers
{
    public partial class LogController : BaseAdminController
    {
        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;

        public LogController(ILogger logger, IWorkContext workContext,
            IPermissionService permissionService)
        {
            this._logger = logger;
            this._workContext = workContext;
            this._permissionService = permissionService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var model = new LogListModel();
            model.AvailableLogLevels = LogLevel.Debug.ToSelectList(false).ToList();
            model.AvailableLogLevels.Insert(0, new SelectListItem { Text = "All", Value = " " });

            return View(model);
        }

        [HttpPost]
        public IActionResult LogList(DataSourceRequest command, LogListModel model)
        {

            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            DateTime? createdOnFromValue = model.CreatedOnFrom;
            if (model.CreatedOnTo != null)
            {
                model.CreatedOnTo = model.CreatedOnTo.Value.AddDays(1);
            }
            DateTime? createdToFromValue = model.CreatedOnTo;

            LogLevel? logLevel = model.LogLevelId > 0 ? (LogLevel?)(model.LogLevelId) : null;

            var logItems = _logger.GetAllLogs(createdOnFromValue, createdToFromValue, model.Message,
                logLevel, command.Page - 1, command.PageSize);

            var gridModel = new DataSourceResult
            {
                Data = logItems.Select(x => new LogModel
                {
                    Id = x.Id,
                    LogLevel = x.LogLevel.ToString(),
                    ShortMessage = x.ShortMessage,
                    FullMessage = "",
                    IpAddress = x.IpAddress,
                    CustomerId = x.CustomerId,
                    PageUrl = x.PageUrl,
                    ReferrerUrl = x.ReferrerUrl,
                    CreatedOn = x.CreatedOnUtc,
                }),
                Total = logItems.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("clearall")]
        public IActionResult ClearAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            _logger.ClearLog();

            SuccessNotification("Cleared");
            return RedirectToAction("List");
        }

        public new IActionResult View(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var log = _logger.GetLogById(id);
            if (log == null)
                //No log found with the specified id
                return RedirectToAction("List");

            var model = new LogModel
            {
                Id = log.Id,
                LogLevel = log.LogLevel.ToString(),
                ShortMessage = log.ShortMessage,
                FullMessage = log.FullMessage,
                IpAddress = log.IpAddress,
                CustomerId = log.CustomerId,
                PageUrl = log.PageUrl,
                ReferrerUrl = log.ReferrerUrl,
                CreatedOn = log.CreatedOnUtc,
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var log = _logger.GetLogById(id);
            if (log == null)
                //No log found with the specified id
                return RedirectToAction("List");

            _logger.DeleteLog(log);


            SuccessNotification("Deleted");
            return RedirectToAction("List");
        }

        [HttpPost]
        public IActionResult DeleteSelected(ICollection<string> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var logItems = _logger.GetLogByIds(selectedIds.ToArray());
                foreach (var logItem in logItems)
                    _logger.DeleteLog(logItem);
            }

            return Json(new { Result = true});
        }
    }
}
