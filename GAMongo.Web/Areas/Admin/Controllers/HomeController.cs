﻿using Microsoft.AspNetCore.Mvc;
using GAMongo.Web.Areas.Admin.Models.Home;
using GAMongo.Core;
using GAMongo.Core.Caching;
using GAMongo.Services.Customers;

namespace GAMongo.Web.Areas.Admin.Controllers
{
    public partial class HomeController : BaseAdminController
    {
        #region Fields

        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;
        private readonly ICustomerService _customerService;

        #endregion

        #region Ctor

        public HomeController(
            IWorkContext workContext,
            ICacheManager cacheManager,
            ICustomerService customerService)
        {
            this._workContext = workContext;
            this._cacheManager= cacheManager;
            this._customerService = customerService;
        }

        #endregion

        #region Utiliti

        private DashboardActivityModel PrepareActivityModel()
        {
            var model = new DashboardActivityModel();
            return model;
        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            var model = new DashboardModel();

            return View(model);
        }

        public IActionResult Statistics()
        {
            var model = new DashboardModel();
            return View(model);
        }

        public IActionResult DashboardActivity()
        {
            var model = PrepareActivityModel();
            return PartialView(model);
        }

        #endregion
    }
}
