﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using GAMongo.Web.Areas.Admin.Models.Customers;
using GAMongo.Services.Customers;
using GAMongo.Services.Directory;
using GAMongo.Services.Security;
using GAMongo.Framework.Kendoui;

namespace GAMongo.Web.Areas.Admin.Controllers
{
    public partial class OnlineCustomerController : BaseAdminController
    {
        #region Fields

        private readonly ICustomerService _customerService;
        private readonly IGeoLookupService _geoLookupService;
        private readonly IPermissionService _permissionService;
        #endregion

        #region Constructors

        public OnlineCustomerController(ICustomerService customerService,
            IGeoLookupService geoLookupService,
            IPermissionService permissionService)
        {
            this._customerService = customerService;
            this._geoLookupService = geoLookupService;
            this._permissionService = permissionService;
        }

        #endregion
        
        #region Methods

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public IActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var customers = _customerService.GetOnlineCustomers(DateTime.UtcNow.AddMinutes(-5),
                null, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = customers.Select(x => new OnlineCustomerModel
                {
                    Id = x.Id,
                    CustomerInfo = !string.IsNullOrEmpty(x.Username) ? x.Username : "Guest",
                    LastIpAddress = x.LastIpAddress,
                    Location = _geoLookupService.LookupCountryName(x.LastIpAddress),
                    LastActivityDate = x.LastActivityDateUtc
                }),
                Total = customers.TotalCount
            };

            return Json(gridModel);
        }

        #endregion
    }
}
